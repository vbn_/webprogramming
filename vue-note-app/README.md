
#Setup dependencies
```
npm install
```

### Start
```
npm run serve
```

### Build
```
npm run build
```